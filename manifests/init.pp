# This manifest is intended to manage studieinfo.lysator.liu.se
class studieinfo {

  $sockdir = 'studieinfo'
  $socket = "/run/${sockdir}/studieinfo.sock"
  $uwsgi_host = 'localhost'
  $uwsgi_port = 8000

  yumrepo { 'chicken_repo':
    ensure   => present,
    name     => 'Chicken',
    descr    => 'Add Chicken Scheme version 5.',
    baseurl  => 'https://download.opensuse.org/repositories/home:/zilti:/chicken/CentOS_8/',
    gpgkey   => 'https://download.opensuse.org/repositories/home:/zilti:/chicken/CentOS_8/repodata/repomd.xml.key',
    gpgcheck => yes,
    enabled  => true,
  }

  # Add lysator-repo
  package { 'lysator-repo':
    ensure   => installed,
    provider => rpm,
    source   => 'http://repomaster/CentOS/lysator-repo-latest.noarch.rpm'
  }

  # Pulled from Lysator's repo. Make sure it's activated
  package { 'studieinfo':
    ensure => latest,
  }


  $dropin = '/etc/systemd/system/studieinfo-web.service.d/override.conf'
  file { dirname($dropin):
    ensure => directory,
  }

  # Needed since nginx.service contains PrivateTmp=true since some
  # version back. Meaning that it couldn't find the UWSGI socket
  # there.
  # That flag could be turned off, but it seems more robust to
  # actually put the socket into /run
  file { $dropin:
    ensure  => file,
    notify  => Service['studieinfo-web'],
    content => @("EOF")
    [Service]
    RuntimeDirectory=${sockdir}
    | EOF
  }

  service { 'studieinfo-web':
    ensure => running,
    enable => true,
  }

  # service { 'studieinfo-fetch.timer':
  #   ensure => running
  # }

  class { '::letsencrypt':
    config  => {
      email => 'octol@lysator.liu.se',
    }
  }

  ensure_packages(['python3-certbot-apache'])

  letsencrypt::certonly { $::fqdn:
    domains           => $::fqdn,
    manage_cron       => true,
    cron_hour         => [1, 13],
    cron_minute       => 56,
    plugin            => 'nginx',
    additional_args   => [ '--quiet', ],
    post_hook_command => [ 'systemctl restart nginx.service', ],
  }

  class { '::nginx':
    proxy_cache_path      => '/tmp/cache',
    proxy_use_temp_path   => 'off',
    # TODO flush this after seacrh backend reload
    proxy_cache_keys_zone => 'search_cache:100m',
    gzip                  => 'on',
    http_tcp_nopush       => 'on',
    http_tcp_nodelay      => 'on',
  }

  nginx::resource::server { $facts['networking']['fqdn']:
    server_name               => [$facts['networking']['fqdn']],
    http2                     => 'on',
    ipv6_enable               => true,
    ipv6_listen_options       => 'default',
    www_root                  => '/usr/share/studieinfo/www',
    index_files               => ['index.html'],
    error_pages               => {
      404 => '/404.html',
    },


    location_cfg_append       => {
      autoindex => 'on',
      try_files => '$uri $uri.html $uri/ =404',
      expires   => '$expires',
    },

    # Encrypt everything
    ssl_redirect              => true,
    ssl                       => true,
    ssl_cert                  => "/etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchain.pem",
    ssl_key                   => "/etc/letsencrypt/live/${facts['networking']['fqdn']}/privkey.pem",
    ssl_session_tickets       => 'off',
    ssl_session_timeout       => '1d',
    ssl_cache                 => 'shared:SSL:50m',
    add_header                => {
      'Strict-Transport-Security' => 'max-age=15768000;includeSubDomains',
      'X-Content-Type-Options'    => 'nosniff',
      'X-Frame-Options'           => 'DENY',
    },

    # Set the paranoia level to 'high'.
    ssl_protocols             => 'TLSv1.2',
    ssl_dhparam               => '/etc/nginx/ssl/dhparams.pem',
    ssl_ecdh_curve            => 'secp521r1:secp384r1',
    ssl_ciphers               => [
      'ECDHE-ECDSA-AES256-GCM-SHA384',
      'ECDHE-RSA-AES256-GCM-SHA384',
      'ECDHE-ECDSA-CHACHA20-POLY1305',
      'ECDHE-RSA-CHACHA20-POLY1305',
      'ECDHE-ECDSA-AES128-GCM-SHA256',
      'ECDHE-RSA-AES128-GCM-SHA256',
      'ECDHE-ECDSA-AES256-SHA384',
      'ECDHE-RSA-AES256-SHA384',
      'ECDHE-ECDSA-AES128-SHA256',
      'ECDHE-RSA-AES128-SHA256',
    ].join(':'),
    ssl_prefer_server_ciphers => 'on',

    # OCSP Stapling ---
    # fetch OCSP records from URL in ssl_certificate and cache them
    ssl_stapling              => true,
    ssl_stapling_verify       => true,

    # verify chain of trust of OCSP response using Root CA and Intermediate certs
    ssl_trusted_cert          => "/etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchain.pem",

    resolver                  => ['130.236.254.225','[2001:6b0:17:f0a0::e1]','130.236.254.4', '[2001:6b0:17:f0a0::4]'],
  }

  ::nginx::resource::location { 'search':
    ensure              => 'present',
    location            => '~* /search/*',
    server              => $facts['networking']['fqdn'],
    ssl_only            => true,
    proxy               => "http://${uwsgi_host}:${uwsgi_port}",

    location_cfg_append => {
      proxy_cache           => 'search_cache',
      proxy_cache_key       => '$scheme$request_method$host$request_uri',
      proxy_cache_methods   => 'GET',
      proxy_cache_lock      => 'on',
      proxy_cache_valid     => '200 10m',
      proxy_cache_use_stale => 'updating',
      proxy_ignore_headers  => 'X-Accel-Expires Expires Cache-Control Set-Cookie',
    },
  }

  ::nginx::resource::map { 'expires':
    ensure   => 'present',
    default  => 'off',
    string   => '$sent_http_content_type',
    mappings => {
      'text/css'   => '28d',
      'text/plain' => '10m',
      'text/html'  => '10m',
      'image/webp' => '28d',
      'image/png'  => '28d',
    },
  }

  ::nginx::resource::server { 'flask':
    listen_ip           => $uwsgi_host,
    listen_port         => $uwsgi_port,

    location_cfg_append => {
      include    => 'uwsgi_params',
      uwsgi_pass => "unix://${socket}",
    },
  }

  file { '/etc/studieinfo/studieinfo.ini':
    ensure  => file,
    content => epp('studieinfo/studieinfo.ini.epp',
      { 'socket' => $socket, }),
  }
}
